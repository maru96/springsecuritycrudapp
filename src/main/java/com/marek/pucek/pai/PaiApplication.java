package com.marek.pucek.pai;

import javax.annotation.PostConstruct;

import com.marek.pucek.pai.repository.UserRepository;
import com.marek.pucek.pai.entity.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class PaiApplication {

	private final UserRepository dao;
	private final PasswordEncoder passwordEncoder;

	public PaiApplication(UserRepository dao, PasswordEncoder passwordEncoder) {
		this.dao = dao;
		this.passwordEncoder = passwordEncoder;
	}

	public static void main(String[] args) {
		SpringApplication.run(PaiApplication.class, args);
	}

	@PostConstruct
	public void init() {
		dao.save(new User("Admin", "Admin", "Admin", passwordEncoder.encode("passwd")));
	}
}
