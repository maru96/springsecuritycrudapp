package com.marek.pucek.pai.repository;

import com.marek.pucek.pai.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
    public User findByLogin(String login);

    public User findByUserid(int id);
}