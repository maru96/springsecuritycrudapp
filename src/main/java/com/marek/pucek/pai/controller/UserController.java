package com.marek.pucek.pai.controller;

import java.security.Principal;

import com.marek.pucek.pai.repository.UserRepository;
import com.marek.pucek.pai.entity.User;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;


@Controller
public class UserController {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository dao;

    public UserController(PasswordEncoder passwordEncoder, UserRepository dao) {
        this.passwordEncoder = passwordEncoder;
        this.dao = dao;
    }

    @GetMapping("/login")
    public String loginPage() {
        //zwrócenie nazwy widoku logowania - login.html
        return "login";
    }

    @GetMapping("/register")
    public String registerPage(Model m) {
        m.addAttribute("user", new User());
        return "register";
    }

    @PostMapping("/register")
    public String registerPagePOST(@ModelAttribute(value = "user") @Valid User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "register";
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        dao.save(user);
        return "redirect:/login";
    }

    @GetMapping("/profile")
    public String profilePage(Model m, Principal principal) {
        m.addAttribute("user", dao.findByLogin(principal.getName()));
        return "profile";
    }

    @GetMapping("/users")
    public String getUsers (Model m) {
        m.addAttribute("users", dao.findAll());
        return "users";
    }

    @GetMapping("/remove")
    public String removeUser(Model m, Principal principal) {
        m.addAttribute("user", dao.findByLogin(principal.getName()));
        return "remove";
    }

    @PostMapping("/remove/{id}")
    public String removeUserPOST(@PathVariable("id") int id) {
        dao.deleteById(id);
        return "redirect:/logout";
    }

    @GetMapping("/edit")
    public String editUser(Model m, Principal principal) {
        User user = dao.findByLogin(principal.getName());
        user.setPassword("");
        m.addAttribute("user", user);
        return "edit";
    }

    @PostMapping("/edit/{id}")
    public String editUserPOST(@ModelAttribute(value = "user") @Valid User user, BindingResult bindingResult, Model m, @PathVariable("id") int id) {
        if (bindingResult.hasErrors()) {
            return "edit";
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Authentication newAuth = new UsernamePasswordAuthenticationToken(user.getLogin(), user.getPassword(), auth.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(newAuth);

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        dao.save(user);

        m.addAttribute("user", dao.findByUserid(id));
        return "profile";
    }

}
